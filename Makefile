TARGET=eanreader

all: $(TARGET)

$(TARGET): main.c
	gcc main.c -o $(TARGET)

clean:
	rm $(TARGET)
